# Create temporal averages of geo-spatial data

This plugin takes geo-spatial data as an input and creates temporal averages and saves tthe result hem in `zarr` format to openstack swift cloud, where it can be easily accessed from anywhere. 

The data input is based on the freva databrowser, meaning that you don't have to give file names as input but select files by their search facets such as `project`, `product`, `model` etc. 
