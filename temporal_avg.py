"""Freva plugin to create temporal averages geo-spatial data."""

from getpass import getuser
from pathlib import Path
import json
import logging
import os
from tempfile import NamedTemporaryFile

from evaluation_system.api import plugin, parameters
from evaluation_system.misc import config, logger


class TempAvg(plugin.PluginAbstract):
    """Create temporal averages."""

    __category__ = "support"
    __tags__ = ["data"]
    __version__ = (2020, 11, 1)
    tool_developer = {"name": "Martin Bergemann", "email": "bergemann@dkrz.de"}
    __short_description__ = "Create temporal averages of data"
    __long_description__ = (
        "This plugin creates temporal averages of "
        "geo-spatial data and uploads the resulting data"
        " to a dkrz swift clout object store in .zarr"
        " format"
    )
    __parameters__ = parameters.ParameterDictionary(
        parameters.String(
            name="time_average",
            help="Set the time average window, monthly, daily etc.",
            mandatory=True,
        ),
        parameters.String(
            name="swift_container",
            help="Name of the swift storage container",
            default="swift_data",
        ),
        parameters.SolrField(
            name="project",
            facet="project",
            help="Data project(s) used to search for input.",
            multiple=True,
            max_items=100,
        ),
        parameters.SolrField(
            name="product",
            facet="product",
            help="Data products(s) used to search for input.",
            multiple=True,
            max_items=100,
        ),
        parameters.SolrField(
            name="model",
            facet="model",
            help="Model(s) used to search for input.",
            multiple=True,
            max_items=100,
        ),
        parameters.SolrField(
            name="institute",
            facet="insittute",
            help="Models institution(s) used to search for input.",
            multiple=True,
            max_items=100,
        ),
        parameters.SolrField(
            name="variable",
            facet="variable",
            help="Data variable(s) used to search for input.",
            multiple=True,
            max_items=100,
        ),
        parameters.SolrField(
            name="time_frequency",
            facet="time_frequency",
            help="Data time_freuqncy(ies) used to search for input.",
            multiple=True,
            max_items=100,
        ),
        parameters.String(
            name="swift_group",
            help=(
                "The name of the container group used to log on to swift."
                "the default is taken from the freva config."
            ),
            default="",
        ),
        parameters.String(
            name="swift_user",
            help=(
                "The name of the container user used to log on to swift."
                "the default is taken from the freva config."
            ),
            default="",
        ),
    )

    def run_tool(self, config_dict):
        """Apply the tool."""

        options = config.get_section("scheduler_options")
        keys = (
            "project",
            "product",
            "model",
            "experiment",
            "time_frequency",
            "institute",
            "variable",
        )
        cfg = {k: config_dict[k] for k in keys if config_dict.get(k)}
        group = options.get("project") or getuser()
        cfg["container"] = config_dict["swift_container"]
        cfg["account"] = config_dict.get("swift_group") or group
        cfg["user"] = config_dict.get("swift_user") or getuser()
        cfg["time_average"] = config_dict["time_average"]
        cfg["swift_key"] = os.environ.get("LC_TELEPHONE", None)
        this_dir = Path(__file__).parent
        cmd = f"python {this_dir / 'assets' / 'run.py'}"
        with NamedTemporaryFile(suffix=".json") as temp_file:
            with open(temp_file.name, "w", encoding="utf-8") as f_obj:
                json.dump(cfg, f_obj)
            if logger.level <= logging.DEBUG:
                debug = "-v"
            else:
                debug = ""
            self.call(f"{cmd} {temp_file.name} {debug}")
