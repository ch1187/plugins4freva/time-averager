"""Install additional resources other than python.

Note: If you want to install python packages use anacanda.
"""
from __future__ import annotations
import argparse
import logging
from pathlib import Path
import subprocess
import sys
from typing import Optional, Union

logging.basicConfig(format="%(name)s - %(levelname)s - %(message)s", level=logging.INFO)
logger: logging.Logger = logging.getLogger(sys.argv[0])


def _install_r_packages(**kwargs) -> None:
    """Install R packages."""
    rexec = Path.cwd() / "plugin_env" / "bin" / "R"
    packages = kwargs["packages"]
    repo_server = kwargs["repo_server"]
    local = kwargs["local"]

    if not rexec.exists():
        raise RuntimeError("You'll need to install R to the plugin environment.")
    if local is True:
        packages = [str(Path(pck).expanduser().absolute()) for pck in packages]
    for pkg in packages:
        if local:
            install_cmd = f'echo "install.packages(\\"{pkg}\\")" | {rexec} --no-save'
        else:
            install_cmd = (
                f'echo "install.packages(\\"{pkg}\\", '
                f'repos=\\"{repo_server}\\")" | {rexec} --no-save'
            )
        subprocess.run(install_cmd, shell=True, check=True)


class ArgParser:
    """Command line interface definition.

    Properties
    ----------
    kwargs: dict[str, Union[str, float, Path]]
            property holding all parsed keyword arguments.
    """

    kwargs: Optional[dict[str, Union[str, bool]]] = None

    def __init__(self) -> None:
        """Instantiate the CLI class."""

        self.parser = argparse.ArgumentParser(
            prog=sys.argv[0],
            description="Install additional resources not available via conda.",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        )
        self.parser.add_argument("-v", "--verbose", action="count", default=0)
        self.subparsers = self.parser.add_subparsers(
            help="Available sub-commands:", required=True,
        )
        self.verbose = 0
        self._add_gnur_parser()

    def _add_gnur_parser(self) -> None:
        """Add cli parser for gnuR"""
        app = self.subparsers.add_parser(
            "gnu-r",
            description="Install R packages (local or remote).",
            help="Install R packages (local or remote).",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        )
        app.add_argument(
            "packages",
            type=str,
            help=(
                "Packages that should be installed, these packages can be "
                "installed from a remote CRAN server or saved locally, if local "
                "packages should be installed then the packages should be the "
                "paths to the packages binaries."
            ),
            nargs="+",
        )
        app.add_argument(
            "--local",
            action="store_true",
            help="Set this flag to install locally stored binaries.",
        )
        app.add_argument(
            "--repo-server",
            type=str,
            default="http://cran.us.r-project.org",
            help=(
                "Repository server from which the packages are installed "
                "Note: This option has no effect once the `--local` option is set"
            ),
        )
        app.set_defaults(apply_func=_install_r_packages)

    @property
    def log_level(self):
        """Get the log level."""
        return max(logging.ERROR - 10 * self.verbose, logging.DEBUG)

    def parse_args(self, argv: list[str]) -> argparse.Namespace:
        """Parse the arguments.

        Parameters
        ----------
        argv: list[str]
              List of command line arguments that is parsed.

        Returns
        -------
        argparse.Namespace
            parsed Namespace object.
        """
        parsed_args = self.parser.parse_args(argv)
        self.kwargs = dict(
            packages=parsed_args.packages,
            local=parsed_args.local,
            repo_server=parsed_args.repo_server,
        )
        self.verbose = parsed_args.verbose
        logger.setLevel(self.log_level)
        return parsed_args


if __name__ == "__main__":
    AP = ArgParser()
    args = AP.parse_args(sys.argv[1:])
    args.apply_func(**AP.kwargs)
