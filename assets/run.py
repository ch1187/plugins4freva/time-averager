"""Main cli of the the plugin."""

from __future__ import annotations
import base64
from collections import defaultdict
import argparse
from getpass import getuser
from functools import cached_property, partial
import json
import logging
import multiprocessing as mp
from multiprocessing.managers import DictProxy
import os
from pathlib import Path
from urllib.parse import urljoin, urlparse
from tempfile import TemporaryDirectory

import freva
from evaluation_system.misc import config
from swift_upload import Swift
from swift_upload.utils import get_client
from swiftclient import client
from tqdm import tqdm
import xarray as xr

logger = logging.getLogger("temp-averager")


def parse_args() -> Path:
    """Argument parser for the main method."""
    cli_app = argparse.ArgumentParser(
        prog="temp-averager",
        description="Calculate temporal averages",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    cli_app.add_argument(
        "input_file",
        type=Path,
        help=(
            "Path to the input json file containing the "
            "config to run the plugin"
        ),
    )
    cli_app.add_argument("-v", "--verbose", action="store_true", default=False)
    args = cli_app.parse_args()
    if args.verbose:
        logger.setLevel(logging.DEBUG)
    return args.input_file


class AvgUploader:
    """Create temporal avg and upload data from the freva databrowser to swift.

    Parameters
    ----------

    time_average: str, default: mon
        the length of the temporal average window
    user: str, default: None
        swift user name, if None given (default) the current user will be
        taken
    account: str, default: None
        swift login group, if None given (default) the user group will be
        taken
    **sear_kw: dict
        Search key words passed to the freva databrowser used for searching
        for data.
    """

    def __init__(
        self,
        container: str,
        time_average: str = "mon",
        user: str | None = None,
        account: str | None = None,
        password: str | None = None,
        **search_kw: str | list[str],
    ):
        self.container = container
        self.time_average = time_average
        self.files = defaultdict(list)
        for data_file in map(Path, freva.databrowser(**search_kw)):
            rel_path = self._get_spec_from_filename(data_file)
            self.files[rel_path].append(data_file)
        self.account = account or getuser()
        self.username = user or getuser()
        _connection = Swift(self.account, self.username, password=password)
        self.auth = _connection.fsspec_auth
        self.container_url = self.create_container(container)
        self.auth_token = _connection.auth_token
        self.storage_url = _connection.storage_url

    def create_container(self, container: str) -> str:
        """Create a public swift container."""
        _connection = Swift(self.account, self.username)
        status = _connection.create_container(container)
        if status not in (201, 202):
            raise ValueError(f"Could not created container {container}")
        _connection.make_public(container)
        return _connection.get_swift_container_url(container)

    @cached_property
    def drs_specs(self) -> list[str]:
        """Get all root directories in the systems drs specs."""
        drs_specs = config.get_drs_config().items()
        return {
            k: Path(s["root_dir"]) for k, s in drs_specs if "root_dir" in s
        }

    def _get_spec_from_filename(self, inp_file: Path) -> Path:
        root_specs = []
        for key, root_dir in self.drs_specs.items():
            root_specs.append(key)
            if inp_file.is_relative_to(root_dir):
                return inp_file.relative_to(root_dir).parent
        raise ValueError(
            f"{inp_file} not in drs specs: {', '.join(root_specs)}"
        )

    @staticmethod
    def create_avg(inp_files: list[Path], avg: str = "mon") -> xr.Dataset:
        """Create a temporal average of a dataset."""
        dset = xr.open_mfdataset(
            sorted(inp_files),
            use_cftime=True,
            parallel=False,
            combine="by_coords",
        )
        return dset.resample(time=avg).mean(keep_attrs=True)

    def average_and_upload_data(self):
        """Apply the average and upload the data."""
        apply_func = partial(
            self._average_and_upload_data,
            swift_account=self.account,
            user=self.username,
            avg=self.time_average,
        )
        apply_func(list(self.files.items())[0])
        return
        with mp.Pool(min(mp.cpu_count(), 20)) as pool:
            list(
                tqdm(
                    pool.imap(apply_func, self.files.items()),
                    total=len(self.files),
                    leave=True,
                    desc="Processing",
                )
            )

    def _average_and_upload_data(
        self,
        paths: tuple[Path, Path],
        swift_account: str | None = None,
        user: str | None = None,
        avg: str = "mon",
    ) -> None:
        """Create temporal averages and upload the data to swift."""
        out_file, inp_files = paths
        out_file = out_file.with_suffix(".zarr")
        with Swift(self.account, self.username) as sw:
            dset = self.create_avg(inp_files, avg=avg)
            client.put_object(
                self.storage_url,
                self.auth_token,
                self.container,
                str(out_file),
                None,
                content_type="application/directory",
            )
            zarr_url = f"{self.container_url}/{out_file}"
            print(zarr_url)
            dset.chunk("auto").to_zarr(
                zarr_url,
                mode="a",
                storage_options={"get_client": get_client, "auth": self.auth},
            )
            container = Path(self.container) / out_file
            # sw.make_public(str(container))


def main(config_file: Path) -> None:
    """Main method to run the plugin."""

    with config_file.open() as f_obj:
        config: dict[str, str | list[str]] = json.load(f_obj)
    container = config.pop("container")
    key = config.pop("swift_key", None)
    user = config.pop("user", None)
    account = config.pop("account", None)
    if key:
        passwd = base64.b64decode(key).decode()
    else:
        passwd = None

    swift_upload = AvgUploader(
        container,
        time_average=config.pop("time_average", "mon"),
        user=user,
        account=account,
        password=passwd,
        **config,
    )
    swift_upload.average_and_upload_data()


if __name__ == "__main__":

    main(parse_args())
